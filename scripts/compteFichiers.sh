if test $# -eq 0
    then
        echo "Vous devez passer des noms de répertoires en paramètres"
    else
        for rep in $*
        do
            if test -d ${rep}
                then
                    echo "Le "${rep}" contient n fichiers réguliers et m sous-répertoires"
                else
                    echo ${rep}" n'est pas un répertoire"
            fi
        done
fi
